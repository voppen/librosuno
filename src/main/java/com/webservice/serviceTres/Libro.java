package com.webservice.serviceTres;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "libros")
public class Libro implements java.io.Serializable
{
	private int libroIsbn;
	private String libroAutor;
	private String libroTitulo;
	private String libroTema;
	
	public Libro()
	{}
	
	public Libro(int isbn, String autor, String titulo,String tema) 
	{
		this.libroIsbn=isbn;
		this.libroAutor=autor;
		this.libroTitulo=titulo;
		this.libroTema=tema;
	}
		
	@Id
	@Column(name = "LIBRO_ISBN", unique = true, nullable = false, precision = 5, scale = 0)
	public int getLibroIsbn() 
	{
		return this.libroIsbn;
	}

	public void setLibroIsbn(int isbn) 
	{
		this.libroIsbn=isbn;
	}
	
	@Column(name = "LIBRO_AUTOR", nullable = false, length = 50)
	public String getLibroAutor() 
	{
		return this.libroAutor;
	}

	public void setLibroAutor(String autor) {
		this.libroAutor = autor;
	}

	@Column(name = "LIBRO_TITULO", nullable = false, length = 50)
	public String getLibroTitulo() 
	{
		return this.libroTitulo;
	}

	public void setLibroTitulo(String titulo) {
		this.libroTitulo = titulo;
	}
	
	@Column(name = "LIBRO_TEMA", nullable = false, length = 50)
	public String getLibroTema() 
	{
		return this.libroTema;
	}

	public void setLibroTema(String tema) {
		this.libroTema = tema;
	}
	
	public String toString() { 
	    return "Isbn: '" + this.libroIsbn + "' Autor: '" + this.libroAutor + "' Titulo: '" + this.libroTitulo + " 'Tema: '" + this.libroTema + "'";
	} 

	
}
