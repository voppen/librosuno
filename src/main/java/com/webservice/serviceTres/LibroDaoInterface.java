package com.webservice.serviceTres;

import java.io.Serializable;
import java.util.List;
import javax.jws.*;



import com.webservice.serviceTres.Libro;

@WebService
public interface LibroDaoInterface 
{
	
	public void guardarLibro(Libro libro);
	
	public Libro buscarLibroPorId(int id);
	
	@WebMethod(operationName="mostrarListaLibros") 
	public List<Libro> buscarListaLibros();
}
