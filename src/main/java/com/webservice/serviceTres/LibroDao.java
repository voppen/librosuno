package com.webservice.serviceTres;

import java.util.List;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.Session;

import javax.jws.*;

import com.webservice.persistence.hibernateUtil;
import com.webservice.serviceTres.Libro;

@WebService(endpointInterface="com.webservice.serviceTres.LibroDaoInterface")
public class LibroDao implements LibroDaoInterface
{
	
	private Session session;
	
	public void LibroDao(){}
	
	public Session getSession()
	{
		BasicConfigurator.configure();   	 	
      	this.session = hibernateUtil.getSessionFactory().openSession();  	            	
        this.session.beginTransaction();       
        return this.session;
	}
		
	@Override
	public void guardarLibro(Libro libro)
	{
		getSession().save(libro);
		session.getTransaction().commit();	
		session.close();
	}
	
	@Override
	public Libro buscarLibroPorId(int id)
	{
		Libro libro = new Libro();
		libro = (Libro)getSession().get(Libro.class, id);
		session.close();
		return libro;
		
	}

	@SuppressWarnings("unchecked")
	public List<Libro> buscarListaLibros()
	{
		List<Libro> libros = getSession().createQuery("from Libro").list();
		session.close();
		return libros;
		
	}
}
